<?php

/**
 * Un fichier de pipelines permet de regrouper
 * les fonctions de branchement du plugin
 * sur des pipelines existants.
 *
 * @plugin    animation
 * @copyright  2024
 * @author     josiane Aletto
 * @licence    GNU/GPL
 * @package    SPIP\paramsite\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
* @pipeline formulaire_charger
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
 function animation_insert_head($flux){	
    $js = find_in_path("javascript/aos.js");
	$flux.= "\n<script type='text/javascript' src='$js'></script>\n";	
	$flux.="\n<script>
    window.onload = function() {
    // code to run animation.
    AOS.init( {
	  offset: 0,         // offset (in px) from the original trigger point
	  delay: 0,            // values from 0 to 3000, with step 50ms
	  duration:1200,      // values from 0 to 3000, with step 50ms
      easing: 'ease',     // default easing for AOS animations
      once: false,      // whether animation should happen only once - while scrolling down
      mirror: true,     // whether elements should animate out while scrolling past them
	 });
};
	</script>\n";	
	 return $flux;
}


 function animation_insert_head_css($flux){
	
	 $flux.="\n".'<link rel="stylesheet" type="text/css" media="all" href="'.find_in_path('css/aos.css').'" />';	
	 $flux.="\n".'<link rel="stylesheet" type="text/css" media="all" href="'.find_in_path('css/aosparam.css').'" />';	
	 return $flux;
}

	 
function animation_head_prive($flux){
	 $flux.="\n".'<link rel="stylesheet" type="text/css" media="all" href="'.find_in_path('css/aos.css').'" />';
	  $flux.="\n".'<link rel="stylesheet" type="text/css" media="all" href="'.find_in_path('css/aosparam.css').'" />';	
	 return $flux;
}
 
